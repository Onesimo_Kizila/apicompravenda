<?php
require_once("kituxiConfig.php");
require_once("kituxiConection.php");
require_once("Kituxi.php");
require_once("RestKituxi.php");
require_once("KituxiRestHandler.php");
$KituxiRestHandler = new KituxiRestHandler();


$controller = isset($_GET["param1"]) ? $_GET["param1"] : "";
$controller = strtolower($controller);


switch ($_SERVER["REQUEST_METHOD"]) {
    case "POST":

        switch ($controller) {


            case "provinciaatualizar":

                $KituxiRestHandler->provinciaAtualizar();

                break;

            case "provinciaeliminar":

                $KituxiRestHandler->provinciaEliminar();

                break;

            case "localizacaoguardar":

                $KituxiRestHandler->localizacaoGuardar();

                break;

            case "localizacaoatualizar":

                $KituxiRestHandler->localizacaoAtualizar();

                break;


            case "produtoguardar":

                $KituxiRestHandler->produtoGuardar();

                break;

            case "produtoatualizar":

                $KituxiRestHandler->produtoAtualizar();

                break;

            case "apresentacaoguardar":

                $KituxiRestHandler->apresentacaoGuardar();

                break;

            case "apresentacaoatualizar":

                $KituxiRestHandler->apresentacaoAtualizar();

                break;

            case "categoriaguardar":

                $KituxiRestHandler->categoriaGuardar();

                break;

            case "categoriaatualizar":

                $KituxiRestHandler->categoriaAtualizar();

                break;

            case "marcaguardar":

                $KituxiRestHandler->marcaGuardar();

                break;

            case "marcaatualizar":

                $KituxiRestHandler->marcaAtualizar();

                break;

            case "impostoguardar":

                $KituxiRestHandler->impostoGuardar();

                break;

            case "impostoatualizar":

                $KituxiRestHandler->impostoAtualizar();

                break;

            case "armazemguardar":

                $KituxiRestHandler->armazemGuardar();

                break;


            case "armazematualizar":

                $KituxiRestHandler->armazemAtualizar();

                break;
        }

    case "GET":
        switch ($controller) {
            case "localizacaomostrar":

                $KituxiRestHandler->localizacaoMostrar();

                break;
            case "localizacaomostrarbyid":

                $KituxiRestHandler->localizacaoMostrarByID();

                break;

            case "categoriamostrar":

                $KituxiRestHandler->categoriaMostrar();

                break;
            case "apresentacaomostrar":

                $KituxiRestHandler->apresentacaoMostrar();

                break;
            case "produtomostrarbycodigo":

                $KituxiRestHandler->produtoMostrarByCodigo();

                break;
            case "produtomostrarbydescricao":

                $KituxiRestHandler->produtoMostrarByDescricao();

                break;
            case "produtomostrarbycodigobarra":

                $KituxiRestHandler->produtoMostrarByCodigoBarra();

                break;
            case "produtobundlemostrarbyproductid":

                $KituxiRestHandler->produtoBundleMostrarByProductID();

                break;
            case "marcamostrar":

                $KituxiRestHandler->marcaMostrar();

                break;

            case "impostomostrar":

                $KituxiRestHandler->impostoMostrar();

                break;
            case "produtomostrar":

                $KituxiRestHandler->produtoMostrar();

                break;

            case "apresentacaomostraractivos":

                $KituxiRestHandler->apresentacaoMostrarActivos();

                break;
            case "categoriamostraractivos":

                $KituxiRestHandler->categoriaMostrarActivos();

                break;

            case "marcamostraractivos":

                $KituxiRestHandler->marcaMostrarActivos();

                break;
            case "impostomostraractivos":

                $KituxiRestHandler->impostoMostrarActivos();

                break;
            case "produtomostrarfilter":

                $KituxiRestHandler->produtoMostrarFilter();

                break;

            case "armazemmostrar":

                $KituxiRestHandler->armazemMostrar();

                break;
        }
        break;
}
