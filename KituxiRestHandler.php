<?php
class KituxiRestHandler extends RestKituxi
{
	//post
	///ApiConsulta/provinciaGuardar

	//post
	///ApiConsulta/provinciaAtualizar
	public function provinciaAtualizar()
	{
		$rawData = $this->__provinciaAtualizar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	//get
	///ApiConsulta/provinciaMostrar
	public function localizacaoMostrar()
	{
		$rawData = $this->__localizacaoMostrar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function localizacaoMostrarByID()
	{
		$rawData = $this->__localizacaoMostrarByID();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	//post
	//ApiConsulta/provinciaEliminar?id=
	public function provinciaEliminar()
	{
		$rawData = $this->__provinciaEliminar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	//post
	//ApiConsulta/provinciaEliminar?id=
	public function localizacaoGuardar()
	{
		$rawData = $this->__localizacaoGuardar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}

	public function localizacaoAtualizar()
	{
		$rawData = $this->__localizacaoAtualizar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	//post

	public function categoriaMostrar()
	{
		$rawData = $this->__categoriaMostrar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}

	public function categoriaMostrarActivos()
	{
		$rawData = $this->__categoriaMostrarActivos();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function marcaMostrarActivos()
	{
		$rawData = $this->__marcaMostrarActivos();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function impostoMostrarActivos()
	{
		$rawData = $this->__impostoMostrarActivos();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function impostoMostrar()
	{
		$rawData = $this->__impostoMostrar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}

	public function apresentacaoMostrar()
	{
		$rawData = $this->__apresentacaoMostrar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function apresentacaoMostrarActivos()
	{
		$rawData = $this->__apresentacaoMostrarActivos();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function armazemGuardar()
	{
		$rawData = $this->__armazemGuardar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function armazemAtualizar()
	{
		$rawData = $this->__armazemAtualizar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function armazemMostrar()
	{
		$rawData = $this->__armazemMostrar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function marcaGuardar()
	{
		$rawData = $this->__marcaGuardar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function marcaAtualizar()
	{
		$rawData = $this->__marcaAtualizar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}

	public function apresentacaoGuardar()
	{
		$rawData = $this->__apresentacaoGuardar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function apresentacaoAtualizar()
	{
		$rawData = $this->__apresentacaoAtualizar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}

	public function categoriaGuardar()
	{
		$rawData = $this->__categoriaGuardar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function categoriaAtualizar()
	{
		$rawData = $this->__categoriaAtualizar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}

	public function impostoGuardar()
	{
		$rawData = $this->__impostoGuardar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function impostoAtualizar()
	{
		$rawData = $this->__impostoAtualizar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function produtoMostrarFilter()
	{
		$rawData = $this->__produtoMostrarFilter();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function marcaMostrar()
	{
		$rawData = $this->__marcaMostrar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function produtoMostrar()
	{
		$rawData = $this->__produtoMostrar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function produtoMostrarByCodigoBarra()
	{
		$rawData = $this->__produtoMostrarByCodigoBarra();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function produtoMostrarByCodigo()
	{
		$rawData = $this->__produtoMostrarByCodigo();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function produtoMostrarByDescricao()
	{
		$rawData = $this->__produtoMostrarByDescricao();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function produtoBundleMostrarByProductID()
	{
		$rawData = $this->__produtoBundleMostrarByProductID();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function produtoAtualizar()
	{
		$rawData = $this->__produtoAtualizar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}
	public function produtoGuardar()
	{
		$rawData = $this->__produtoGuardar();
		$this->setHttpHeaders($_SERVER['HTTP_ACCEPT'], 300);
		echo $this->encodeJson($rawData);
	}

	public function encodeJson($responseData)
	{
		$jsonResponse = json_encode($responseData, JSON_UNESCAPED_UNICODE);
		return $jsonResponse;
	}
}
