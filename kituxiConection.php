<?php 
class kituxiConection{
	private static $conn;
	public function __construct(){}

	public static function conn(){

		try{
			if(is_null(self::$conn))
				self::$conn = new PDO('mysql:host='.HOST.';dbname='.BD.'', ''.USER.'', ''.PASS.'',
					array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
				);
				self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		catch(PDOException $e){
			return false;
		}

		return self::$conn;	

	}
}
