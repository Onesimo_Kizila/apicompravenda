<?php
class Kituxi extends kituxiConection
{

    public function __construct()
    {
    }

    public function __provinciaGuardar()
    {
        $resultinfo = "";
        try {
            $object = json_decode(file_get_contents("php://input"), true);

            $myPdo = (new self)->conn();

            $titulo = isset($object["titulo"]) ? $object["titulo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $criado_user = isset($object["criado_user"]) ? $object["criado_user"] : "";

            if (!isset($object["titulo"]))
                return $this->messageRequired("titulo");
            else
                if ($object["titulo"] == "")
                return $this->messageRequired("titulo");

            if (!isset($object["criado_user"]))
                return $this->messageRequired("criado_user");
            if ($object["criado_user"] == "")
                return  $this->messageRequired("criado_user");

            $stmt = $myPdo->prepare("SELECT * FROM tbl_provincia WHERE titulo=?");
            $stmt->execute([$titulo]);

            if ($stmt->rowCount() > 0) {
                return "provincia " . $object["titulo"] . " ja existe!";
            }

            $sql = "INSERT INTO `tbl_provincia` ( `titulo`, `descricao`, `criado_user`, `criado_data`, `atualizado_user`, `atualizado_data`) VALUES ( ?, ?, ?, now(), ?, now());";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$titulo, $descricao, $criado_user, $criado_user]);

            $resultinfo = "sucesso";
        } catch (Exception $e) {
            $resultinfo = $e;
        }
        return $resultinfo;
    }

    public function __provinciaAtualizar()
    {
        $resultinfo = "";
        try {
            $object = json_decode(file_get_contents("php://input"), true);

            $myPdo = (new self)->conn();

            $provincia_id = isset($object["provincia_id"]) ? $object["provincia_id"] : 0;
            $titulo = isset($object["titulo"]) ? $object["titulo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $atualizado_user = isset($object["atualizado_user"]) ? $object["atualizado_user"] : "";

            $stmt = $myPdo->prepare("SELECT * FROM tbl_provincia WHERE provincia_id=?");
            $stmt->execute([$provincia_id]);

            if (($provincia_id == 0) || ($stmt->rowCount() == 0)) {
                return "provincia_id é invalida";
            }

            if (!isset($object["titulo"]))
                return $this->messageRequired("titulo");
            else
                if ($object["titulo"] == "")
                return $this->messageRequired("titulo");

            if (!isset($object["atualizado_user"]))
                return $this->messageRequired("atualizado_user");
            if ($object["atualizado_user"] == "")
                return $this->messageRequired("atualizado_user");

            $sql = "UPDATE `tbl_provincia` SET `titulo` = ?, `descricao` = ? , `atualizado_user` = ? , `atualizado_data` = NOW() WHERE `provincia_id` = ?;";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$titulo, $descricao, $atualizado_user, $provincia_id]);

            $resultinfo = "sucesso";
        } catch (Exception $e) {
            $resultinfo = $e;
        }
        return $resultinfo;
    }

    public function __localizacaoGuardar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();


            $localizacao_parent = isset($object["localizacao_parent"]) ? $object["localizacao_parent"] : null;
            $codigo = isset($object["codigo"]) ? $object["codigo"] : "";
            $titulo = isset($object["titulo"]) ? $object["titulo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $criado_user = isset($object["criado_user"]) ? $object["criado_user"] : "";
            //$activo = isset($object["activo"]) ? $object["activo"] : "";

            if (!isset($object["codigo"]))
                return $this->Error($object, 10, $this->messageRequired("codigo"));
            else
        if ($object["codigo"] == "")
                return $this->Error($object, 10, $this->messageRequired("codigo"));


            if (!isset($object["titulo"]))
                return $this->Error($object, 10, $this->messageRequired("titulo"));
            else
            if ($object["titulo"] == "")
                return $this->Error($object, 10, $this->messageRequired("titulo"));


            $stmt = $myPdo->prepare("SELECT * FROM com_localizacao WHERE codigo=?");
            $stmt->execute([$codigo]);

            if ($stmt->rowCount() > 0) {
                return $this->Error($object, 10, $this->messageJaExiste($object["codigo"]));
            }

            $sql = "INSERT INTO `com_localizacao` ( `localizacao_parent`,`codigo`,`titulo`, `descricao`, `criado_user`, `criado_data`, `atualizado_user`, `atualizado_data`,`activo` ) VALUES ( ?,?,?, ?, ?, now(), ?, now(),1);";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$localizacao_parent, $codigo, $titulo, $descricao, $criado_user, $criado_user]);

            $object["localizacao_id"] = $myPdo->lastInsertId();

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }
    public function __localizacaoAtualizar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();

            $localizacao_id = isset($object["localizacao_id"]) ? $object["localizacao_id"] : 0;
            $localizacao_parent = isset($object["localizacao_parent"]) ? $object["localizacao_parent"] : null;
            $codigo = isset($object["codigo"]) ? $object["codigo"] : "";
            $titulo = isset($object["titulo"]) ? $object["titulo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $atualizado_user = isset($object["atualizado_user"]) ? $object["atualizado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : "";


            if (!isset($object["codigo"]))
                return $this->Error($object, 10, $this->messageRequired("codigo"));
            else
        if ($object["codigo"] == "")
                return $this->Error($object, 10, $this->messageRequired("codigo"));


            if (!isset($object["titulo"]))
                return $this->Error($object, 10, $this->messageRequired("titulo"));
            else
            if ($object["titulo"] == "")
                return $this->Error($object, 10, $this->messageRequired("titulo"));


            $stmt = $myPdo->prepare("SELECT * FROM com_localizacao WHERE localizacao_id=?");
            $stmt->execute([$localizacao_id]);

            if (($localizacao_id == 0) || ($stmt->rowCount() == 0)) {
                return $this->Error($object, 10, "localizacao_id é inválida");
            }
            $sql = "UPDATE `com_localizacao` SET `localizacao_parent` = ?, `codigo` = ? , `titulo` = ? , `descricao` = ?, `atualizado_user` = ?  , `atualizado_data` = NOW(), `activo` = b? WHERE `localizacao_id` = ?;";

            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$localizacao_parent, $codigo, $titulo, $descricao, $atualizado_user, $activo, $localizacao_id]);

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }
    public function __apresentacaoGuardar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();

            $titulo = isset($object["titulo"]) ? $object["titulo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $criado_user = isset($object["criado_user"]) ? $object["criado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : 0;

            if (!isset($object["titulo"]))
                return $this->Error($object, 10, $this->messageRequired("titulo"));
            else
            if ($object["titulo"] == "")
                return $this->Error($object, 10, $this->messageRequired("titulo"));


            $stmt = $myPdo->prepare("SELECT * FROM tbl_apresentacao WHERE titulo=?");
            $stmt->execute([$titulo]);

            if ($stmt->rowCount() > 0) {
                return $this->Error($object, 10, $this->messageJaExiste($object["titulo"]));
            }

            $sql = "INSERT INTO `tbl_apresentacao` ( `titulo`, `descricao`, `criado_user`, `criado_data`, `atualizado_user`, `atualizado_data`,`activo` ) VALUES ( ?,?, ?, now(), ?, now(),b?);";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$titulo, $descricao, $criado_user, $criado_user, $activo]);

            $object["apresentacao_id"] = $myPdo->lastInsertId();

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }


    public function __apresentacaoAtualizar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();
            $apresentacao_id = isset($object["apresentacao_id"]) ? $object["apresentacao_id"] : 0;
            $titulo = isset($object["titulo"]) ? $object["titulo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $atualizado_user = isset($object["atualizado_user"]) ? $object["atualizado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : 0;

            if (!isset($object["titulo"]))
                return $this->Error($object, 10, $this->messageRequired("titulo"));
            else
            if ($object["titulo"] == "")
                return $this->Error($object, 10, $this->messageRequired("titulo"));


            $stmt = $myPdo->prepare("SELECT * FROM tbl_apresentacao WHERE apresentacao_id=?");
            $stmt->execute([$apresentacao_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageInvalidID($apresentacao_id));
            }

            $sql = "UPDATE `tbl_apresentacao` SET `titulo` = ?, `descricao` = ?, `atualizado_user` = ?, `atualizado_data` = NOW(),`activo` = b? WHERE apresentacao_id = ? ;";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$titulo, $descricao, $atualizado_user, $activo, $apresentacao_id]);

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }

    public function __marcaGuardar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();

            $titulo = isset($object["titulo"]) ? $object["titulo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $criado_user = isset($object["criado_user"]) ? $object["criado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : 0;

            if (!isset($object["titulo"]))
                return $this->Error($object, 10, $this->messageRequired("titulo"));
            else
            if ($object["titulo"] == "")
                return $this->Error($object, 10, $this->messageRequired("titulo"));


            $stmt = $myPdo->prepare("SELECT * FROM tbl_marca WHERE titulo=?");
            $stmt->execute([$titulo]);

            if ($stmt->rowCount() > 0) {
                return $this->Error($object, 10, $this->messageJaExiste($object["titulo"]));
            }

            $sql = "INSERT INTO `tbl_marca` ( `titulo`, `descricao`, `criado_user`, `criado_data`, `atualizado_user`, `atualizado_data`,`activo` ) VALUES ( ?,?, ?, now(), ?, now(),b?);";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$titulo, $descricao, $criado_user, $criado_user, $activo]);

            $object["marca_id"] = $myPdo->lastInsertId();

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }

    public function __marcaAtualizar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();
            $marca_id = isset($object["marca_id"]) ? $object["marca_id"] : 0;
            $titulo = isset($object["titulo"]) ? $object["titulo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $atualizado_user = isset($object["atualizado_user"]) ? $object["atualizado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : 0;

            if (!isset($object["titulo"]))
                return $this->Error($object, 10, $this->messageRequired("titulo"));
            else
            if ($object["titulo"] == "")
                return $this->Error($object, 10, $this->messageRequired("titulo"));


            $stmt = $myPdo->prepare("SELECT * FROM tbl_marca WHERE marca_id=?");
            $stmt->execute([$marca_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageInvalidID($marca_id));
            }

            $sql = "UPDATE `tbl_marca` SET `titulo` = ?, `descricao` = ?, `atualizado_user` = ?, `atualizado_data` = NOW(),`activo` = b? WHERE marca_id = ? ;";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$titulo, $descricao, $atualizado_user, $activo, $marca_id]);

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }

    public function __armazemGuardar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();

            $codigo = isset($object["codigo"]) ? $object["codigo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $ordem = isset($object["ordem"]) ? $object["ordem"] : 0;
            $vende = isset($object["vende"]) ? $object["vende"] : 0;
            $criado_user = isset($object["criado_user"]) ? $object["criado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : 0;

            if (!isset($object["codigo"]))
                return $this->Error($object, 10, $this->messageRequired("codigo"));
            else
            if ($object["codigo"] == "")
                return $this->Error($object, 10, $this->messageRequired("codigo"));


            $stmt = $myPdo->prepare("SELECT * FROM tbl_armazem WHERE codigo=?");
            $stmt->execute([$codigo]);

            if ($stmt->rowCount() > 0) {
                return $this->Error($object, 10, $this->messageJaExiste($object["codigo"]));
            }

            $sql = "INSERT INTO `tbl_armazem` (`armazem_id`, `codigo`, `descricao`, `ordem`, `vende`, `activo`, `criado_user`, `criado_data`, `atualizado_user`, `atualizado_data`, `apagado`) VALUES (NULL, ?, ?, ?, b?, b?, ?, NOW(), ?, NOW(), b'0');";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$codigo, $descricao, $ordem, $vende, $activo, $criado_user, $criado_user]);

            $object["armazem_id"] = $myPdo->lastInsertId();

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }
    public function __armazemAtualizar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();
            $armazem_id = isset($object["armazem_id"]) ? $object["armazem_id"] : 0;
            $codigo = isset($object["codigo"]) ? $object["codigo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $ordem = isset($object["ordem"]) ? $object["ordem"] : 0;
            $vende = isset($object["vende"]) ? $object["vende"] : 0;
            $atualizado_user = isset($object["atualizado_user"]) ? $object["atualizado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : 0;

            if (!isset($object["codigo"]))
                return $this->Error($object, 10, $this->messageRequired("codigo"));
            else
            if ($object["codigo"] == "")
                return $this->Error($object, 10, $this->messageRequired("codigo"));


            $stmt = $myPdo->prepare("SELECT * FROM tbl_armazem WHERE armazem_id=?");
            $stmt->execute([$armazem_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageInvalidID($armazem_id));
            }

            $sql = "UPDATE `tbl_armazem` SET `codigo` = ?, `descricao` = ?, `ordem` = ?, `vende` = b?, `activo` = b?, `atualizado_user` = ?, `atualizado_data` = NOW() WHERE armazem_id = ? ;";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$codigo, $descricao, $ordem, $vende, $activo, $atualizado_user, $armazem_id]);

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }

    public function __armazemMostrar()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT * FROM tbl_armazem WHERE apagado = 0 order by atualizado_data desc;");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }
    public function __categoriaGuardar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();

            $titulo = isset($object["titulo"]) ? $object["titulo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $criado_user = isset($object["criado_user"]) ? $object["criado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : 0;

            if (!isset($object["titulo"]))
                return $this->Error($object, 10, $this->messageRequired("titulo"));
            else
            if ($object["titulo"] == "")
                return $this->Error($object, 10, $this->messageRequired("titulo"));


            $stmt = $myPdo->prepare("SELECT * FROM tbl_categoria WHERE titulo=?");
            $stmt->execute([$titulo]);

            if ($stmt->rowCount() > 0) {
                return $this->Error($object, 10, $this->messageJaExiste($object["titulo"]));
            }

            $sql = "INSERT INTO `tbl_categoria` ( `titulo`, `descricao`, `criado_user`, `criado_data`, `atualizado_user`, `atualizado_data`,`activo` ) VALUES ( ?,?, ?, now(), ?, now(),b?);";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$titulo, $descricao, $criado_user, $criado_user, $activo]);

            $object["categoria_id"] = $myPdo->lastInsertId();

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }



    public function __categoriaAtualizar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();
            $categoria_id = isset($object["categoria_id"]) ? $object["categoria_id"] : 0;
            $titulo = isset($object["titulo"]) ? $object["titulo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $atualizado_user = isset($object["atualizado_user"]) ? $object["atualizado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : 0;

            if (!isset($object["titulo"]))
                return $this->Error($object, 10, $this->messageRequired("titulo"));
            else
            if ($object["titulo"] == "")
                return $this->Error($object, 10, $this->messageRequired("titulo"));


            $stmt = $myPdo->prepare("SELECT * FROM tbl_categoria WHERE categoria_id=?");
            $stmt->execute([$categoria_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageInvalidID($categoria_id));
            }

            $sql = "UPDATE `tbl_categoria` SET `titulo` = ?, `descricao` = ?, `atualizado_user` = ?, `atualizado_data` = NOW(),`activo` = b? WHERE categoria_id = ? ;";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$titulo, $descricao, $atualizado_user, $activo, $categoria_id]);

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }


    public function __localizacaoMostrar()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT * FROM com_localizacao");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }


    public function __localizacaoMostrarByID()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT * FROM com_localizacao WHERE localizacao_id = ? LIMIT 1");
        $seleciona->execute([$_GET["id"]]);
        $result = $seleciona->fetchAll();
        return $result[0];
    }

    public function __categoriaMostrar()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT * FROM tbl_categoria order by atualizado_data desc");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }
    public function __categoriaMostrarActivos()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT * FROM tbl_categoria WHERE activo = 1 and apagado = 0 order by atualizado_data desc;");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }
    public function __marcaMostrarActivos()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT * FROM tbl_marca WHERE activo = 1 and apagado = 0 order by atualizado_data desc;");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }
    public function __impostoMostrarActivos()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT * FROM tbl_imposto WHERE activo = 1 and apagado = 0 order by atualizado_data desc;");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }

    public function __apresentacaoMostrar()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT * FROM tbl_apresentacao order by atualizado_data desc");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }


    public function __apresentacaoMostrarActivos()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT * FROM tbl_apresentacao WHERE activo = 1 and apagado = 0 order by atualizado_data desc;");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }

    public function __marcaMostrar()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT * FROM tbl_marca order by atualizado_data desc");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }

    public function __impostoMostrar()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT * FROM tbl_imposto order by atualizado_data desc");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }
    public function __produtoBundleMostrarByProductID()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT b.product_id_bundle,b.product_id,b.product_id_bundle_include,p.codigo as codigo_bundle,p.descricao as descricao_bundle,b.preco,b.quantidade,b.criado_data,b.criado_user,b.atualizado_data,b.atualizado_user,b.activo FROM tbl_produto_bundle as b join tbl_produto as p on b.product_id_bundle_include = p.product_id WHERE b.product_id = ?  order by b.product_id_bundle");
        $seleciona->execute([$_GET["id"]]);
        $result = $seleciona->fetchAll();
        return $result;
    }
    public function __impostoGuardar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();

            $codigo = isset($object["codigo"]) ? $object["codigo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $criado_user = isset($object["criado_user"]) ? $object["criado_user"] : "";
            $percentagem = isset($object["percentagem"]) ? $object["percentagem"] : 0;
            $padrao = isset($object["padrao"]) ? $object["padrao"] : 0;
            $activo = isset($object["activo"]) ? $object["activo"] : 1;

            if (!isset($object["codigo"]))
                return $this->Error($object, 10, $this->messageRequired("codigo"));
            else
            if ($object["codigo"] == "")
                return $this->Error($object, 10, $this->messageRequired("codigo"));


            $stmt = $myPdo->prepare("SELECT * FROM tbl_imposto WHERE codigo=?");
            $stmt->execute([$codigo]);

            if ($stmt->rowCount() > 0) {
                return $this->Error($object, 10, $this->messageJaExiste($object["codigo"]));
            }

            $sql = "INSERT INTO `tbl_imposto` ( `codigo`, `descricao`, `padrao`, `percentagem`, `criado_user`, `criado_data`, `atualizado_user`, `atualizado_data`,`activo` ) VALUES ( ?,?,b?,?, ?, now(), ?, now(),b?);";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$codigo, $descricao, $padrao, $percentagem, $criado_user, $criado_user, $activo]);

            $object["imposto_id"] = $myPdo->lastInsertId();

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }
    public function __impostoAtualizar()
    {
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];

            $myPdo = (new self)->conn();
            $imposto_id = isset($object["imposto_id"]) ? $object["imposto_id"] : 0;
            $titulo = isset($object["codigo"]) ? $object["codigo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : null;
            $percentagem = isset($object["percentagem"]) ? $object["percentagem"] : 0;
            $padrao = isset($object["padrao"]) ? $object["padrao"] : 0;
            $atualizado_user = isset($object["atualizado_user"]) ? $object["atualizado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : 0;

            if (!isset($object["codigo"]))
                return $this->Error($object, 10, $this->messageRequired("codigo"));
            else
            if ($object["codigo"] == "")
                return $this->Error($object, 10, $this->messageRequired("codigo"));


            $stmt = $myPdo->prepare("SELECT * FROM tbl_imposto WHERE imposto_id=?");
            $stmt->execute([$imposto_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageInvalidID($imposto_id));
            }

            $sql = "UPDATE `tbl_imposto` SET `descricao` = ?, `percentagem` = ?, `padrao` = b?, `atualizado_user` = ?, `atualizado_data` = NOW(),`activo` = b? WHERE imposto_id = ? ;";
            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$descricao, $percentagem, $padrao, $atualizado_user, $activo, $imposto_id]);

            return $this->OK($object);
        } catch (Exception $ex) {
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }
    public function __produtoGuardar()
    {
        $myPdo = (new self)->conn();
        $object = json_decode(file_get_contents("php://input"), true)["Item"];

        try {


            $imposto_id = isset($object["imposto_id"]) ? $object["imposto_id"] : 0;
            $marca_id = isset($object["marca_id"]) ? $object["marca_id"] : 0;
            $categoria_id = isset($object["categoria_id"]) ? $object["categoria_id"] : 0;
            $apresentacao_id = isset($object["apresentacao_id"]) ? $object["apresentacao_id"] : 0;
            $codigo_barra = isset($object["codigo_barra"]) ? $object["codigo_barra"] : "";
            $codigo = isset($object["codigo"]) ? $object["codigo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : "";
            $preco = isset($object["preco"]) ? $object["preco"] : 0;
            $bundle = isset($object["bundle"]) ? $object["bundle"] : 0;
            $controla_serial_no = isset($object["controla_serial_no"]) ? $object["controla_serial_no"] : 0;
            $move_stock = isset($object["move_stock"]) ? $object["move_stock"] : 0;
            $tipo_artigo = isset($object["tipo_artigo"]) ? $object["tipo_artigo"] : 0;
            $criado_user = isset($object["criado_user"]) ? $object["criado_user"] : "";
            $atualizado_user = isset($object["atualizado_user"]) ? $object["atualizado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : 0;
            $apagado = isset($object["apagado"]) ? $object["apagado"] : 0;

            if (!isset($object["codigo"]))
                return $this->Error($object, 10, $this->messageRequired("codigo"));
            else
            if ($object["codigo"] == "")
                return $this->Error($object, 10, $this->messageRequired("codigo"));


            if ($criado_user == "")
                return $this->Error($object, 10, $this->messageRequired("criado_user"));

            if ($atualizado_user == "")
                return $this->Error($object, 10, $this->messageRequired("atualizado_user"));


            if (!isset($object["descricao"]))
                return $this->Error($object, 10, $this->messageRequired("descricao"));
            else
            if ($descricao == "")
                return $this->Error($object, 10, $this->messageRequired("descricao"));

            //verificar se produto ja existe 
            $stmt = $myPdo->prepare("SELECT * FROM tbl_produto WHERE codigo=?");
            $stmt->execute([$codigo]);

            if ($stmt->rowCount() > 0) {
                return $this->Error($object, 10, $this->messageJaExiste($object["codigo"]));
            }

            //verificar se imposto_id existe

            $stmt = $myPdo->prepare("SELECT * FROM tbl_imposto WHERE imposto_id =?");
            $stmt->execute([$imposto_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageRelacaoInvalida("imposto_id"));
            }

            //verificar se marca_id existe

            $stmt = $myPdo->prepare("SELECT * FROM tbl_marca WHERE marca_id =?");
            $stmt->execute([$marca_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageRelacaoInvalida("marca_id"));
            }

            //verificar se categoria_id existe

            $stmt = $myPdo->prepare("SELECT * FROM tbl_categoria WHERE categoria_id =?");
            $stmt->execute([$categoria_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageRelacaoInvalida("categoria_id"));
            }

            //verificar se apresentacao_id existe

            $stmt = $myPdo->prepare("SELECT * FROM tbl_apresentacao WHERE apresentacao_id =?");
            $stmt->execute([$apresentacao_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageRelacaoInvalida("apresentacao_id"));
            }

            $sql = "START TRANSACTION;";
            $sql = $sql . "SELECT  @orderNumber:=MAX(product_id)+1 FROM tbl_produto;";
            $sql = $sql . "INSERT INTO `tbl_produto` (`product_id`, `imposto_id`, `marca_id`, `categoria_id`, `apresentacao_id`, `codigo_barra`, `codigo`, `descricao`, `preco`, `bundle`, `controla_serial_no`, `move_stock`, `tipo_artigo`, `criado_user`, `criado_data`, `atualizado_user`, `atualizado_data`, `activo`, `apagado`) ";
            $sql = $sql . "VALUES (@orderNumber, ?, ?, ?, ?, ?, ?, ?, ?, b?, b?, b?, b?, ?, NOW(), ?, NOW(), b?, b?);";

            $product_bundle = $object["product_bundle"];


            foreach ($product_bundle as $bundleItem) {
                $sql = $sql . "INSERT INTO `tbl_produto_bundle` (`product_id`, `product_id_bundle_include`, `preco`, `quantidade`, `criado_user`, `criado_data`, `atualizado_user`, `atualizado_data`, `activo`)";
                $sql = $sql . "VALUES (@orderNumber," . $bundleItem["product_id_bundle_include"] . "," . $bundleItem["preco"] . "," . $bundleItem["quantidade"] . ",'" . $criado_user . "',NOW(),'" . $criado_user . "',NOW(), b'1');";
            }

            $sql = $sql . "COMMIT;";

            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$imposto_id, $marca_id, $categoria_id, $apresentacao_id, $codigo_barra, $codigo, $descricao, $preco, $bundle, $controla_serial_no, $move_stock, $tipo_artigo, $criado_user, $atualizado_user, $activo, $apagado]);

            $stmt->closeCursor();
            $stmt = $myPdo->query("SELECT MAX(product_id) FROM tbl_produto;");
            $object["product_id"] = $stmt->fetchColumn();

            return $this->OK($object);
        } catch (Exception $ex) {

            $stmt = $myPdo->prepare("ROLLBACK;");
            $stmt->execute();
            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine() . "product_id:" . $object["product_id"]);
        }
    }

    //atualizar produto
    public function __produtoAtualizar()
    {
        $myPdo = (new self)->conn();
        try {
            $object = json_decode(file_get_contents("php://input"), true)["Item"];


            $produto_id = isset($object["product_id"]) ? $object["product_id"] : 0;
            $imposto_id = isset($object["imposto_id"]) ? $object["imposto_id"] : 0;
            $marca_id = isset($object["marca_id"]) ? $object["marca_id"] : 0;
            $categoria_id = isset($object["categoria_id"]) ? $object["categoria_id"] : 0;
            $apresentacao_id = isset($object["apresentacao_id"]) ? $object["apresentacao_id"] : 0;
            $codigo_barra = isset($object["codigo_barra"]) ? $object["codigo_barra"] : "";
            $codigo = isset($object["codigo"]) ? $object["codigo"] : "";
            $descricao = isset($object["descricao"]) ? $object["descricao"] : "";
            $preco = isset($object["preco"]) ? $object["preco"] : 0;
            $bundle = isset($object["bundle"]) ? $object["bundle"] : 0;
            $controla_serial_no = isset($object["controla_serial_no"]) ? $object["controla_serial_no"] : 0;
            $move_stock = isset($object["move_stock"]) ? $object["move_stock"] : 0;
            $tipo_artigo = isset($object["tipo_artigo"]) ? $object["tipo_artigo"] : 0;
            $criado_user = isset($object["criado_user"]) ? $object["criado_user"] : "";
            $atualizado_user = isset($object["atualizado_user"]) ? $object["atualizado_user"] : "";
            $activo = isset($object["activo"]) ? $object["activo"] : 0;
            $apagado = isset($object["apagado"]) ? $object["apagado"] : 0;

            //verificar se produto ja existe 
            $stmt = $myPdo->prepare("SELECT * FROM tbl_produto WHERE product_id=?");
            $stmt->execute([$produto_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageInvalidID("produto_id" . $produto_id));
            }

            if (!isset($object["codigo"])) {
                return $this->Error($object, 10, $this->messageRequired("codigo"));
            } else
            if ($object["codigo"] == "") {
                return $this->Error($object, 10, $this->messageRequired("codigo"));
            }

            if ($criado_user == "")
                return $this->Error($object, 10, $this->messageRequired("criado_user"));

            if ($atualizado_user == "")
                return $this->Error($object, 10, $this->messageRequired("atualizado_user"));


            if (!isset($object["descricao"]))
                return $this->Error($object, 10, $this->messageRequired("descricao"));
            else
            if ($descricao == "")
                return $this->Error($object, 10, $this->messageRequired("descricao"));



            //verificar se imposto_id existe

            $stmt = $myPdo->prepare("SELECT * FROM tbl_imposto WHERE imposto_id =?");
            $stmt->execute([$imposto_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageRelacaoInvalida("imposto_id"));
            }

            //verificar se marca_id existe

            $stmt = $myPdo->prepare("SELECT * FROM tbl_marca WHERE marca_id =?");
            $stmt->execute([$marca_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageRelacaoInvalida("marca_id"));
            }

            //verificar se categoria_id existe

            $stmt = $myPdo->prepare("SELECT * FROM tbl_categoria WHERE categoria_id =?");
            $stmt->execute([$categoria_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageRelacaoInvalida("categoria_id"));
            }

            //verificar se apresentacao_id existe

            $stmt = $myPdo->prepare("SELECT * FROM tbl_apresentacao WHERE apresentacao_id =?");
            $stmt->execute([$apresentacao_id]);

            if ($stmt->rowCount() == 0) {
                return $this->Error($object, 10, $this->messageRelacaoInvalida("apresentacao_id"));
            }


            $sql = "START TRANSACTION;";
            $sql = $sql . "UPDATE `tbl_produto` SET `imposto_id` = ?, `marca_id` = ? , `categoria_id` = ? , `apresentacao_id` = ?, `codigo_barra` = ?  , `descricao` = ?, `preco` = ?, `bundle` = b?, `controla_serial_no` = b?, `move_stock` = b?, `tipo_artigo` = b?, `atualizado_user` = ?, `atualizado_data` = NOW(), `activo` = b?, `apagado` = b? WHERE `product_id` = ?;";

            $product_bundle = $object["product_bundle"];


            foreach ($product_bundle as $bundleItem) {
                if ($bundleItem["product_id_bundle"] == 0) {
                    $sql = $sql . "INSERT INTO `tbl_produto_bundle` (`product_id`, `product_id_bundle_include`, `preco`, `quantidade`, `criado_user`, `criado_data`, `atualizado_user`, `atualizado_data`, `activo`)";
                    $sql = $sql . "VALUES (" . $produto_id . "," . $bundleItem["product_id_bundle_include"] . "," . $bundleItem["preco"] . "," . $bundleItem["quantidade"] . ",'" . $criado_user . "',NOW(),'" . $criado_user . "',NOW(), b'1');";
                } else if ($bundleItem["apagado"] == 0)
                    $sql = $sql . "UPDATE `tbl_produto_bundle` SET  `quantidade` = " . $bundleItem["quantidade"] . ", `atualizado_user` = '" . $criado_user . "', `atualizado_data` = NOW() WHERE product_id_bundle = " . $bundleItem["product_id_bundle"] . ";";
                else
                    $sql = $sql . "DELETE FROM tbl_produto_bundle WHERE product_id_bundle=" . $bundleItem["product_id_bundle"] . ";";
            }

            $sql = $sql . "COMMIT;";

            $stmt = $myPdo->prepare($sql);

            $stmt->execute([$imposto_id, $marca_id, $categoria_id, $apresentacao_id, $codigo_barra, $descricao, $preco, $bundle, $controla_serial_no, $move_stock, $tipo_artigo, $atualizado_user, $activo, $apagado, $produto_id]);

            return $this->OK($object);
        } catch (Exception $ex) {

            $stmt = $myPdo->prepare("ROLLBACK;");
            $stmt->execute();

            return $this->Error($object, 0, "Exception:" . $ex->getMessage() . "\nLineError:" . $ex->getLine());
        }
    }
    public function __produtoMostrar()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT p.product_id,p.imposto_id,i.percentagem as imposto,p.marca_id,m.titulo as marca,p.categoria_id,c.titulo as categoria,p.apresentacao_id,a.titulo as apresentacao,p.codigo_barra,p.codigo,p.descricao,p.preco,p.bundle,p.controla_serial_no,p.move_stock,p.tipo_artigo,p.criado_user,p.criado_data,p.atualizado_user,p.atualizado_data,p.activo,p.apagado FROM `tbl_produto` as p 
        inner join tbl_imposto as i on p.imposto_id = i.imposto_id 
        inner JOIN tbl_marca as m on p.marca_id = m.marca_id
        inner JOIN tbl_categoria as c on p.categoria_id = c.categoria_id
        inner JOIN tbl_apresentacao as a on p.apresentacao_id = a.apresentacao_id
        order by p.atualizado_data desc LIMIT 50;");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }

    public function __produtoMostrarFilter()
    {
        $tc = false;
        $where = "WHERE ";
        $codigo = isset($_GET["codigo"]) ? $_GET["codigo"] : "";
        $descricao = isset($_GET["descricao"]) ? $_GET["descricao"] : "";
        $categoria_id = isset($_GET["categoria_id"]) ? $_GET["categoria_id"] : 0;
        $apresentacao_id = isset($_GET["apresentacao_id"]) ? $_GET["apresentacao_id"] : 0;
        $marca_id = isset($_GET["marca_id"]) ? $_GET["marca_id"] : 0;

        if ($codigo != '') {
            $where = $where . "p.codigo LIKE '%" . $codigo . "%' ";
            $tc = true;
        }
        if ($descricao != '') {
            $where = $tc ? ($where . "and ") : $where;
            $where = $where . "p.descricao LIKE '%" . $descricao . "%' ";
            $tc = true;
        }
        if ($categoria_id != 0) {
            $where = $tc ? ($where . "and ") : $where;
            $where = $where . "p.categoria_id = " . $categoria_id . " ";
            $tc = true;
        }

        if ($apresentacao_id != 0) {
            $where = $tc ? ($where . "and ") : $where;
            $where = $where . "p.apresentacao_id = " . $apresentacao_id . " ";
            $tc = true;
        }

        if ($marca_id != 0) {
            $where = $tc ? ($where . "and ") : $where;
            $where = $where . "p.marca_id = " . $marca_id . " ";
            $tc = true;
        }

        if ($where == "WHERE ") {
            $where = '';
        }

        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT p.product_id,p.imposto_id,i.percentagem as imposto,p.marca_id,m.titulo as marca,p.categoria_id,c.titulo as categoria,p.apresentacao_id,a.titulo as apresentacao,p.codigo_barra,p.codigo,p.descricao,p.preco,p.bundle,p.controla_serial_no,p.move_stock,p.tipo_artigo,p.criado_user,p.criado_data,p.atualizado_user,p.atualizado_data,p.activo,p.apagado FROM `tbl_produto` as p 
        inner join tbl_imposto as i on p.imposto_id = i.imposto_id 
        inner JOIN tbl_marca as m on p.marca_id = m.marca_id
        inner JOIN tbl_categoria as c on p.categoria_id = c.categoria_id
        inner JOIN tbl_apresentacao as a on p.apresentacao_id = a.apresentacao_id " . $where . "
        order by p.atualizado_data desc " . ($where == '' ? "LIMIT 50" : "") . ";");
        $seleciona->execute();
        $result = $seleciona->fetchAll();
        return $result;
    }

    public function __produtoMostrarByCodigoBarra()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT p.product_id,p.imposto_id,i.percentagem as imposto,p.marca_id,m.titulo as marca,p.categoria_id,c.titulo as categoria,p.apresentacao_id,a.titulo as apresentacao,p.codigo_barra,p.codigo,p.descricao,p.preco,p.bundle,p.controla_serial_no,p.move_stock,p.tipo_artigo,p.criado_user,p.criado_data,p.atualizado_user,p.atualizado_data,p.activo,p.apagado FROM `tbl_produto` as p 
        inner join tbl_imposto as i on p.imposto_id = i.imposto_id 
        inner JOIN tbl_marca as m on p.marca_id = m.marca_id
        inner JOIN tbl_categoria as c on p.categoria_id = c.categoria_id
        inner JOIN tbl_apresentacao as a on p.apresentacao_id = a.apresentacao_id 
        WHERE p.activo = 1 and p.apagado = 0 and p.codigo_barra LIKE ? order by p.atualizado_data desc;");
        $seleciona->execute(["%" . $_GET["codigo_barra"] . "%"]);
        $result = $seleciona->fetchAll();
        return $result;
    }

    public function __produtoMostrarByCodigo()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT p.product_id,p.imposto_id,i.percentagem as imposto,p.marca_id,m.titulo as marca,p.categoria_id,c.titulo as categoria,p.apresentacao_id,a.titulo as apresentacao,p.codigo_barra,p.codigo,p.descricao,p.preco,p.bundle,p.controla_serial_no,p.move_stock,p.tipo_artigo,p.criado_user,p.criado_data,p.atualizado_user,p.atualizado_data,p.activo,p.apagado FROM `tbl_produto` as p 
        inner join tbl_imposto as i on p.imposto_id = i.imposto_id 
        inner JOIN tbl_marca as m on p.marca_id = m.marca_id
        inner JOIN tbl_categoria as c on p.categoria_id = c.categoria_id
        inner JOIN tbl_apresentacao as a on p.apresentacao_id = a.apresentacao_id 
        WHERE p.activo = 1 and p.apagado = 0 and p.codigo LIKE ? order by p.atualizado_data desc;");
        $seleciona->execute(["%" . $_GET["codigo"] . "%"]);
        $result = $seleciona->fetchAll();
        return $result;
    }

    public function __produtoMostrarByDescricao()
    {
        $myPdo = (new self)->conn();
        $seleciona = $myPdo->prepare("SELECT p.product_id,p.imposto_id,i.percentagem as imposto,p.marca_id,m.titulo as marca,p.categoria_id,c.titulo as categoria,p.apresentacao_id,a.titulo as apresentacao,p.codigo_barra,p.codigo,p.descricao,p.preco,p.bundle,p.controla_serial_no,p.move_stock,p.tipo_artigo,p.criado_user,p.criado_data,p.atualizado_user,p.atualizado_data,p.activo,p.apagado FROM `tbl_produto` as p 
        inner join tbl_imposto as i on p.imposto_id = i.imposto_id 
        inner JOIN tbl_marca as m on p.marca_id = m.marca_id
        inner JOIN tbl_categoria as c on p.categoria_id = c.categoria_id
        inner JOIN tbl_apresentacao as a on p.apresentacao_id = a.apresentacao_id 
        WHERE  p.activo = 1 and p.apagado = 0 and p.descricao LIKE ? order by p.atualizado_data desc;");
        $seleciona->execute(["%" . $_GET["descricao"] . "%"]);
        $result = $seleciona->fetchAll();
        return $result;
    }

    public function __provinciaEliminar()
    {
        $result = "";
        $count = 0;
        try {
            if (isset($_POST["id"])) {
                $myPdo = (new self)->conn();
                $del = $myPdo->prepare("DELETE FROM tbl_provincia WHERE provincia_id=?");
                $del->execute([$_POST["id"]]);
                $count = $del->rowCount();
            } else {
                return $this->messageParameter("id");
            }

            $result = $count > 0 ? "sucesso" : "error";
        } catch (Exception $e) {
            $result = $e;
        }
        return $result;
    }

    public function messageRequired($field)
    {
        return "o campo " . $field . " é obrigatorio";
        exit();
    }
    public function messageInvalidID($field)
    {
        return "o id " . $field . " é inválido";
        exit();
    }

    public function messageParameter($parameter)
    {
        return "parametro " . $parameter . " em falta";
        exit();
    }
    public function messageJaExiste($parameter)
    {
        return "esse registro ( " . $parameter . " ) ja foi efetuado";
        exit();
    }
    public function messageRelacaoInvalida($parameter)
    {
        return "conflito com a relacao do campo ( " . $parameter . " ) ";
        exit();
    }
    public function OK($item)
    {
        $result["Subject"] = "";
        $result["Time"] = "";
        $result["StatusCode"] = 200;
        $result["TagCode"] = 1;
        $result["Description"] = "{0} Criado Com sucesso";
        $result["Item"] = $item;
        return $result;
        exit();
    }

    public function Error($item, $tagCode, $description)
    {
        $result["Subject"] = "";
        $result["Time"] = "";
        $result["StatusCode"] = 409;
        $result["TagCode"] = $tagCode;
        $result["Description"] = $description;
        $result["Item"] = $item;
        return $result;

        exit();
    }
}
